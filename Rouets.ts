import {Router} from "https://deno.land/x/oak/mod.ts"
import { getproducts,getproduct,addproduct,updateproduct,deleteproduct } from "./products.ts";
const router=new Router()


router.get('/api/v1/products', getproducts)
    .get('/api/v1/products/:id', getproduct)
    .post('/api/v1/products', addproduct)
    .put('/api/v1/products/:id', updateproduct)
    .delete('/api/v1/products/:id', deleteproduct)

// router.get('/api/v1/products', ({response} : {response:any})=>{
//     response.body='hello word'
// })
export default router